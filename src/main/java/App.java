import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatLightLaf;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.RefineryUtilities;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;


public class App {

    private JPanel mainPanel;
    private JButton q1Button;
    private JButton q2Button;
    private JButton q3Button;
    private JButton q4Button;
    private JButton nowaGraButton;
    private JButton a50Na50Button;
    private JButton telefonButton;
    private JButton publicznoscButton;
    private JTextArea questionLabel;
    private JPanel toolbarPanel;

    private int correctAnswer = 0;
    private int numOfQuestion = 1;

    Random rnd = new Random();

    // - "DODATKI" DO GUI -
    public App(){

        q1Button.addActionListener(new ButtonListener());
        q2Button.addActionListener(new ButtonListener());
        q3Button.addActionListener(new ButtonListener());
        q4Button.addActionListener(new ButtonListener());
        nowaGraButton.addActionListener(new ButtonListener());
        a50Na50Button.addActionListener(new ButtonListener());
        telefonButton.addActionListener(new ButtonListener());
        publicznoscButton.addActionListener(new ButtonListener());


        // - PRZYCISKI DOMYSLNI ZABLOKOWANE -
        q1Button.setEnabled(false);
        q2Button.setEnabled(false);
        q3Button.setEnabled(false);
        q4Button.setEnabled(false);

        a50Na50Button.setEnabled(false);
        telefonButton.setEnabled(false);
        publicznoscButton.setEnabled(false);
    }

    // - FUNKCJA MAIN -
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel( new FlatDarculaLaf() );
        } catch( Exception ex ) {
            System.err.println( "Failed to initialize LaF" );
        }

        JFrame frame = new JFrame("App");
        frame.setContentPane(new App().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(650,400);

        frame.setVisible(true);
        frame.setResizable(false);
    }

    // - LISTENER PRZYCISKÓW -
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            // - NOWA GRA -
            if(e.getSource() == nowaGraButton) {

                a50Na50Button.setEnabled(true);
                telefonButton.setEnabled(true);
                publicznoscButton.setEnabled(true);

                numOfQuestion = 1;
                getQuestions();
            }

            // - PRZYCISKI PYTAN -
            if(e.getSource() == q1Button) {checkAnswer(1);}
            if(e.getSource() == q2Button) {checkAnswer(2);}
            if(e.getSource() == q3Button) {checkAnswer(3);}
            if(e.getSource() == q4Button) {checkAnswer(4);}

            // - KOŁA RATUNKOWE -
            if(e.getSource() == a50Na50Button) {twoAnswers();}
            if(e.getSource() == telefonButton) {askFriend();}
            if(e.getSource() == publicznoscButton) {askPublic();}
        }
    }

    // - ODPOWIEDZI PUBLICZNOSCI -
    private void askPublic() {

        ArrayList<Integer> answers = new ArrayList<>();

        if (q1Button.isEnabled() == true) {answers.add(1);}
        if (q2Button.isEnabled() == true) {answers.add(2);}
        if (q3Button.isEnabled() == true) {answers.add(3);}
        if (q4Button.isEnabled() == true) {answers.add(4);}

        publicznoscButton.setEnabled(false);
        BarChart chart = new BarChart("Publiczność","Która odpowiedź poprawna?",correctAnswer,answers);
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
    }

    // - TELEFON DO PRZYJACIELA -
    private void askFriend() {
        // - JEŻELI PYTANIE <6 ZAWSZE POPRAWNE -
        // - ELSE LOSOWA ODPOWIEDŹ -

        int friendHint;
        if (numOfQuestion < 6)
        {
            friendHint = correctAnswer;
        } else {

            ArrayList<Integer> answers = new ArrayList<>();

            if (q1Button.isEnabled() == true) {answers.add(1);}
            if (q2Button.isEnabled() == true) {answers.add(2);}
            if (q3Button.isEnabled() == true) {answers.add(3);}
            if (q4Button.isEnabled() == true) {answers.add(4);}

            int rand = rnd.nextInt(answers.size());
            friendHint = answers.get(rand);
        }
        telefonButton.setEnabled(false);
        JOptionPane.showMessageDialog(new JFrame(),"Hmm... mysle, ze to numer " + friendHint, "Telefon",JOptionPane.PLAIN_MESSAGE);
    }

    // - ODRZUCANIE 2 NIEPOPRAWNYCH ODPOWIEDZI -
    private void twoAnswers() {

        // - TWORZENIE LISTY Z BŁĘDNYMI ODPOWIEDZIAMI -
        ArrayList<Integer> wrongQuestions = new ArrayList<Integer>();
        for (int i=1;i<=4;i++) {
            if (i != correctAnswer) {wrongQuestions.add(i);}
        }

        // - LOSOWANIE I USUWANIE BŁĘDNEGO PYTANIA -
        for (int i=1; i<=2; i++)
        {
            int toDelete = (int)(Math.random() * wrongQuestions.size());

            switch (wrongQuestions.get(toDelete)) {
                case 1 : q1Button.setEnabled(false);break;
                case 2 : q2Button.setEnabled(false);break;
                case 3 : q3Button.setEnabled(false);break;
                case 4 : q4Button.setEnabled(false);break;
                default: break;
            }

            wrongQuestions.remove(toDelete);
        }

        a50Na50Button.setEnabled(false);

    }

    // - SPRAWDZANIE ODPOWIEDZI -
    private void checkAnswer(int i) {
        if (i == correctAnswer)
        {
            numOfQuestion++;
            // - CZY OSTANIE PYTANIE -
            if (playerWin() == false) {
                // - NASTEPNE PYTANIE -
                getQuestions();
            }
        } else {
            // - KONIEC GRY -
            JOptionPane.showMessageDialog(new JFrame(),"Nie poprawna odpowiedź", "Koniec Gry",JOptionPane.ERROR_MESSAGE);
            q1Button.setEnabled(false);
            q2Button.setEnabled(false);
            q3Button.setEnabled(false);
            q4Button.setEnabled(false);

            a50Na50Button.setEnabled(false);
            telefonButton.setEnabled(false);
            publicznoscButton.setEnabled(false);
        }
    }

    private boolean playerWin() {
        if (numOfQuestion >= 13) {
            JOptionPane.showMessageDialog(new JFrame(),"Wygrana! Gratulacje!", "Koniec Gry",JOptionPane.PLAIN_MESSAGE);
            q1Button.setEnabled(false);
            q2Button.setEnabled(false);
            q3Button.setEnabled(false);
            q4Button.setEnabled(false);

            a50Na50Button.setEnabled(false);
            telefonButton.setEnabled(false);
            publicznoscButton.setEnabled(false);
            return true;
        } else {return false;}
    }

    // - USTAWIANIE PYTAN NA GUI -
    private void getQuestions() {

        try {
            StringBuffer response = getRequest();
            JSONObject myObj = new JSONObject(response.toString());


            questionLabel.setText("Pytanie nr " + numOfQuestion + ": " + myObj.getString("question"));
            correctAnswer   = myObj.getInt("correctAnswer");

            JSONArray   answers         = myObj.getJSONArray("answers");
            q1Button.setText(answers.getString(0));
            q2Button.setText(answers.getString(1));
            q3Button.setText(answers.getString(2));
            q4Button.setText(answers.getString(3));

            // - PONOWNA MOŻLIWOŚĆ NACISIECIA PRZYCISKOW -
            q1Button.setEnabled(true);
            q2Button.setEnabled(true);
            q3Button.setEnabled(true);
            q4Button.setEnabled(true);

        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error: Brak połączenia");
            a50Na50Button.setEnabled(false);
            telefonButton.setEnabled(false);
            publicznoscButton.setEnabled(false);

            q1Button.setEnabled(false);
            q2Button.setEnabled(false);
            q3Button.setEnabled(false);
            q4Button.setEnabled(false);

        }


    }


    // - POBIERANIE PYTAN -
    public StringBuffer getRequest() throws IOException {
        try {

            URL url = new URL("http://127.0.0.1:8080/millionaire/question/" + numOfQuestion);
            HttpURLConnection urlCon=(HttpURLConnection)url.openConnection();
            urlCon.setRequestMethod("GET");
            String line="";
            InputStreamReader isr = new InputStreamReader(urlCon.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            StringBuffer response = new StringBuffer();
            while ((line=br.readLine())!=null){
                response.append(line);
            }

            br.close();
            return response;

        } catch (Exception e) {
            throw e;
        }
    }


}
