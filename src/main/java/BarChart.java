import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Random;

public class BarChart extends JFrame {
    public BarChart(String title, String chartTitle,int correctAnswer,ArrayList answers) {

        super(title);
        JFreeChart barChart = ChartFactory.createBarChart(
                chartTitle,
                "Nr. pytania",
                "Procent",
                createDataSet(correctAnswer,answers),
                PlotOrientation.VERTICAL,
                true,true,false);

        CategoryPlot plot = barChart.getCategoryPlot();
        plot.setBackgroundPaint(new Color(40,40,40));
        plot.setRangeGridlinePaint(new Color(170,170,170));

        Color column = new Color(76,80,82);

        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0,column);
        renderer.setSeriesPaint(1,column);
        renderer.setSeriesPaint(2,column);
        renderer.setSeriesPaint(3,column);
        renderer.setDrawBarOutline(true);

        ChartFrame frame = new ChartFrame("Wyniki Głosowania",barChart);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setBackground(new Color (17,17,17));
        chartPanel.setPreferredSize(new java.awt.Dimension(560,367));
        setContentPane(chartPanel);
    }

    private CategoryDataset createDataSet(int correctAnswer, ArrayList<Integer> answers) {
        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset();

        Random rnd = new Random();

        int [] procents = {0,0,0,0};
        int procentLeft = 100;

        procents[correctAnswer-1] = rnd.nextInt(50)+50;
        procentLeft -= procents[correctAnswer-1];

        /*
        for(int i=0;i<3;i++) {
            if (procents[i] == 0){
                procents[i] = rnd.nextInt(procentLeft);
                procentLeft -= procents[i];
            }
        }

        procents[3] += procentLeft;
         */

        for (int i : answers)
        {
            if(procents[i-1] == 0) {
                procents[i-1] = rnd.nextInt(procentLeft);
                procentLeft -= procents [i-1];
            }
        }

        procents[answers.get(0)-1] += procentLeft;


        dataset.addValue(procents[0],"1","Odpowiedź");
        dataset.addValue(procents[1],"2","Odpowiedź");
        dataset.addValue(procents[2],"3","Odpowiedź");
        dataset.addValue(procents[3],"4","Odpowiedź");

        return dataset;
    }
}
